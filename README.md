# Installation

### Dependices

 * python

### Installation

Just download the python file and run it. You may have to edit the file to use desktop background manager you perfer.

# Configuration

### Display Time
Chang the valeu of the varible "show_time" near the top of the file to the number of seconds the script will show the file for. The defalt valeu is 60 seconds.

### Command To Change The Desktop Backgound

Chang the valeu of the varible "ba_command" near the top of the file to the command to change the background. Makesure to add a space on the end of the valeu. the path of the file will be added to the end of the command. The defalt command is feh --bg-fill.

### Set The Derictory That The Pictures Are In

Chang the valeu of the varible "dire" near the top of the file to the derictory where youre files are stored. The defalt valeu is "~/Pictures/backgrounds/'.
 Note: The script only displays .jpg files (not case censitive)
