#! /usr/bin/python3

'''
Copyright (c) 2016 Aren Moynihan

Email: aren@ctgaia.net
'''

import subprocess as sub
import re
import os
import time
import argparse
import sys

''' this script finds all the jpg files in the spesifyed derictory
then cylices through them as the background image using the command pcmanfm -w path of image file '''


''' Config '''
parser = argparse.ArgumentParser(description='Change the desktop background')
parser.add_argument('-f', '--folder',
                    default='~/Pictures/backgrounds/',
                    help='Folder with desktop backgrounds in it. Default ~/Pictures/backgrounds/')
parser.add_argument('-t', '--time',
                    default='60',
                    type=int,
                    help='Time to show the background for in seconds, the default is 60.')
parser.add_argument('-c', '--command',
                    default='feh --bg-fill',
                    help='command used to change the background, the default is feh --bg-fill.')
args = parser.parse_args()

# the time to show each image fore
show_time=args.time
# The command to use to change the desktop background
## makesure to add a space at the end
ba_command=args.command
ba_command=ba_command + ' '
# derictory where the files are stored
## only files that end in .jpg (not case centisive) will be seen by the program
folder=args.folder

def get_img():
    ''' get a list of image files in the derictory specified in the folder varable '''
    #sub.call('pkill /home/aren/scripts/desktop-background-photo-change.py', shell=True)
    global al1
    global a
    a = sub.check_output('cd ' + folder + '; ls | grep -i .jpg', shell=True)
    al = re.split(r'\\n', str(a))
    al1 = []

    ''' makesure the only things in the list of the files are image files '''
    for x in range(len(al)):
        al2 = re.findall(r'\w+\w+.*', al[x])
        al1 = al1 + al2
get_img()
print('theese are the files that will be used: ' + str(al1))

while True:
    an = sub.check_output('cd ' + folder + '; ls | grep -i .jpg', shell=True)
    ''' update file list if the files in the derictery change '''
    if an != a:
        print('updating the files in the dirctory')
        get_img()
        print('theese are the new files: ' + str(al1))
    ''' cycle through the background image files in the dirctery '''
    for y in range(len(al1)):
        y1 = y-1
        os.system(ba_command + folder + str(al1[y1]))
        time.sleep(show_time)
